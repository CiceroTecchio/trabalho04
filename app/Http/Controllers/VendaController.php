<?php

namespace App\Http\Controllers;

use App\Produto;
use Illuminate\Http\Request;
use App\Venda;

class VendaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        try{
            return response()->json( [Venda::with('produto')->paginate(5)->all()], 200 );
        }catch( \Exception $e ){
            return response()->json( ["mensagem" => $e->getMessage()], 500 );
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(
        $request,
        [
            'produto_id' => 'numeric | required | min:1',
            'qtd' => 'numeric | required | min:1'
        ]
    );

        $valorut = Produto::find($request->input('produto_id'))->valor;
        $venda = new Venda();
        $venda->fill($request->all() );
        $qtd = $venda->qtd;
        $venda->total = $valorut * $qtd;

        $venda->save();
        if( $venda ){
            return response()->json( [$venda], 201 );
        }else{
            return response()->json( ["mensagem" => "Erro ao cadastrar venda"], 400 );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $venda = Venda::with('produto')->find($id);

        return $venda;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate(
            $request,
            [
                'produto_id' => 'numeric | min:1',
                'qtd' => 'numeric | min:1'
            ]
        );
        $venda = Venda::find($id);

        $venda->fill($request->all() );
        $valorut = Produto::find($request->input('produto_id'))->valor;
        $venda->total = $venda->qtd * $valorut;
        $venda->save();
        return $venda;

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $venda = Venda::find($id);

        $venda->delete();

        return Venda::all();
    }
}
