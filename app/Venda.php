<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Venda extends Model
{
    protected $fillable = ['produto_id', 'qtd', 'total'];

    public function produto(){
        return $this->belongsTo('App\Produto', 'produto_id', 'id');
    }
}
