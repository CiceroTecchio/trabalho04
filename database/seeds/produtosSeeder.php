<?php

use Illuminate\Database\Seeder;
use App\Produto;

class produtosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        produto::create([
           'nome' => 'vassoura',
           'valor' => 10.50,
        ]);

        produto::create([
            'nome' => 'creme dental',
            'valor' => 3.50,
        ]);

        produto::create([
            'nome' => 'arroz',
            'valor' => 13.00,
        ]);

        produto::create([
            'nome' => 'feijao',
            'valor' => 5.00,
        ]);

        produto::create([
            'nome' => 'pão',
            'valor' => 0.35,
        ]);
    }
}
