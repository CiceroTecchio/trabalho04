<?php

use Illuminate\Database\Seeder;
use App\Venda;

class vendasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        venda::create([
            'produto_id' => '1',
            'qtd' => 3,
            'total' => 31.50,
        ]);

        venda::create([
            'produto_id' => '2',
            'qtd' => 5,
            'total' => 17.50,
        ]);

        venda::create([
            'produto_id' => '3',
            'qtd' => 3,
            'total' => 39.00,
        ]);

        venda::create([
            'produto_id' => '4',
            'qtd' => 10,
            'total' => 50.00
        ]);

        venda::create([
            'produto_id' => '5',
            'qtd' => 15,
            'total' => 5.25
        ]);


    }
}
